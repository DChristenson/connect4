let cells = document.getElementsByClassName("cell");
let columns = document.getElementsByClassName("column");
let currentPlayer = "Red";
let nextPlayer = "Black";
let victory = document.getElementById("results");
let gameWinner = false;

let emptyBoard = [[], [], [], [], [], [], []];

//code to set multple attributes provided by DMG below
const setAttributes = (element, attributes = {}) => {
  for (let [name, value] of Object.entries(attributes)) {
    element.setAttribute(name, value);
  }
  return element;
};

// Example Use:
//   const div = document.createElement("div")
//   setAttributes(div, { class: "cell", id: "cell-01" })

//generate grid
function createBoard(x) {
  let table = document.createElement("table");
  let tableBody = document.createElement("tbody");
  for (let i = 0; i < 7; i++) {
    let tr = table.insertRow();
    tr.addEventListener("click", playerTurn);
    tr.setAttribute("class", "column  " + (i + 1));

    for (let j = 0; j < 6; j++) {
      let td = document.createElement("td");
      setAttributes(td, {
        class: "cell-" + j,
        id: "row " + (j + 1),
      });
      tr.appendChild(td);
    }
  }
  table.appendChild(tableBody);
  document.body.appendChild(table);
}

createBoard(emptyBoard);

//creates player disks
function createDisk(player) {
  let disk = document.createElement("div");
  disk.setAttribute("id", player);
  return disk;
}

let pieceCount = 0;

//picks lowest locations for disks and changes player turn
function playerTurn(event) {
  let playerDisk = createDisk(currentPlayer);
  let pickedColumn = Array.from(event.currentTarget.children);
  let pickedNumber = event.currentTarget.classList[1] - 1;
  if (pickedColumn[5].hasChildNodes()) {
    event.currentTarget.removeEventListener("click", playerTurn);
    return;
  }
  for (let i = 0; i < pickedColumn.length; i++) {
    if (!pickedColumn[i].hasChildNodes()) {
      pickedColumn[i].appendChild(playerDisk);
      pieceCount++;
      break;
    }
  }
  if (currentPlayer == "Red") {
    emptyBoard[pickedNumber].push(1);
    nextPlayer = "Black";
  } else {
    nextPlayer = "Red";
    emptyBoard[pickedNumber].push(2);
  }
  checkWinner(currentPlayer);
  if (gameWinner == true) {
    gameOver();
  }
  currentPlayer = nextPlayer;
  if (pieceCount == 42 && gameWinner == false) {
    victory.innerHTML = "It's a tie! Try Harder!";
  }
}

//check win conditions
function checkVertical(currentPlayer) {
  for (let i = 0; i < emptyBoard.length; i++) {
    let column = emptyBoard[i];
    for (let j = 0; j < column.length; j++) {
      let cell = column[j];
      let cell2 = column[j + 1];
      let cell3 = column[j + 2];
      let cell4 = column[j + 3];
      if (
        cell === cell2 &&
        cell2 === cell3 &&
        cell3 === cell4 &&
        gameWinner == false
      ) {
        victoryStatement();
      }
    }
  }
}

function checkHorizonal(currentPlayer) {
  for (let k = 0; k < emptyBoard.length - 3; k++) {
    let column = emptyBoard[k];
    for (let l = 0; l < column.length; l++) {
      let pick = emptyBoard[k][l];
      let pick2 = emptyBoard[k + 1][l];
      let pick3 = emptyBoard[k + 2][l];
      let pick4 = emptyBoard[k + 3][l];
      if (
        pick === pick2 &&
        pick2 === pick3 &&
        pick3 === pick4 &&
        gameWinner == false
      ) {
        victoryStatement();
      }
    }
  }
}

function checkDiagonalUp(currentPlayer) {
  for (let i = 0; i < emptyBoard.length - 3; i++) {
    let column = emptyBoard[i];
    for (let j = 0; j < column.length; j++) {
      let selected = emptyBoard[i][j];
      let selected2 = emptyBoard[i + 1][j + 1];
      let selected3 = emptyBoard[i + 2][j + 2];
      let selected4 = emptyBoard[i + 3][j + 3];
      if (
        selected === selected2 &&
        selected2 === selected3 &&
        selected3 === selected4 &&
        gameWinner == false
      ) {
        victoryStatement();
      }
    }
  }
}

function checkDiagonalDown(currentPlayer) {
  for (let i = 3; i < emptyBoard.length; i++) {
    let column = emptyBoard[i];
    for (let j = 0; j < column.length; j++) {
      let selected = emptyBoard[i][j];
      let selected2 = emptyBoard[i - 1][j + 1];
      let selected3 = emptyBoard[i - 2][j + 2];
      let selected4 = emptyBoard[i - 3][j + 3];
      if (
        selected === selected2 &&
        selected2 === selected3 &&
        selected3 === selected4 &&
        gameWinner == false
      ) {
        victoryStatement();
      }
    }
  }
}

function checkWinner(currentPlayer) {
  checkDiagonalDown(currentPlayer);
  checkDiagonalUp(currentPlayer);
  checkHorizonal(currentPlayer);
  checkVertical(currentPlayer);
  if (!gameWinner) {
    document.getElementById(
      "results"
    ).innerHTML = `<h3>${nextPlayer}, it's your turn!</h3>`;
  }
}

//removes event listeners from columns
function gameOver() {
  for (let i = 0; i < columns.length; i++) {
    columns[i].removeEventListener("click", playerTurn);
  }
}
//changes innerHTML of victory div to declare winner
function victoryStatement() {
  victory.innerHTML =
    "<h3>Congratulations, " + currentPlayer + "! You have won!</h3>";
  gameWinner = true;
}
